# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Chat Room
* Key functions (add/delete)
    1. chat
    2. load message history
    3. chat with new user
* Other functions (add/delete)
    1. [xxx]
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|   
|Other functions|1~10%|N|

## Website Detail Description
* 登入畫面(背景是影片)
    
    <img src="/images/111.png" >


** 登入畫面功能

1. Sign up:在打完帳號密碼後，可點取下面的
   sign up now，就可成功註冊。

2. Login :若已是會員，點取下面LOGIN，即可進入聊天室。

3. FB Login:可用Facebook登入。

4. Google Login:可用Google登入(須按圖片才可)。


* 聊天室(背景是影片)

<img src="/images/112.png" >


** 聊天室畫面功能

1. Chrome notification:

<img src="/images/113.png" >

當成功進入聊天室後，右下角會有notification。

2. 下面的白框框可輸入Name和Text，按送出訊息後，就會出現在聊天室裡。

3. 左上角可logout回到登入介面。

* Template

https://colorlib.com/etc/lf/Login_v11/index.html 登入畫面

https://tutorials.webduino.io/zh-tw/docs/socket/useful/im-1.html 聊天室

* RWD

這是我用手機開網頁的登入頁面。

<img src="/images/115.jpg" >

這是用手機進入聊天室後的聊天頁面。

<img src="/images/116.jpg" >

在這邊給的寬度都是device的寬度, 所以當用不同裝置使用時，只需向下拉不用左右移動,畫面的比例和看到的東西都會一致。

* CSS Animation

我的一開始登入頁面的Login button，有animation。

## Security Report (Optional)

SSL，即安全通訊端層，簡而言之，這是一種標準的技術，用於保持網際網路連線安全以及防止在兩個系統之間發送的所有敏感資料被罪犯讀取及修改任何傳輸的資訊，包括潛在的個人詳細資料。兩個系統可以是伺服器與用戶端 (例如購物網站與瀏覽器)，或者伺服器至伺服器 (例如，含有個人身份資訊或含有薪資資訊的應用程式)。

這樣做是為了確保使用者與網站、或兩個系統之間傳輸的任何資料保持無法被讀取的狀態。此技術可使用加密演算法以混淆輸送中的資料，防止駭客在資料透過連線發送時讀取資料。此資訊可能是任何敏感或個人資訊，包括信用卡號與其他財務資訊、姓名與地址。

HTTPS (Hyper Text Transfer Protocol Secure，超級文字傳輸協議安全) 會在網站受到 SSL 憑證保護時在網址中出現。該憑證的詳細資料包括發行機構與網站擁有人的企業名稱，可以透過按一下瀏覽器列上的鎖定標記進行檢視，所以當網址開頭是https的話，代表它有一定的安全性。
而這是我deploy的gitlab網址https://105062243.gitlab.io/Midterm_Project。

另外只有登入帳號才能使用我的聊天室，才能讀取和使用我的firebase。

<img src="/images/114.png" >

