document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function Loginsuccessful() {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Chat Room', {
            icon: 'images/notification.jpg',
            body: "Login Successfully,\nHope you be delighted in my chatroom :)",
        });
        notification.onclick = function () {
            window.location.replace('chatroom.html');
        };
    }
}